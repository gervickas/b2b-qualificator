import { StrictMode } from "react";
import * as ReactDOM from "react-dom";
import ShellRouter from "./Router";

ReactDOM.render(
  <StrictMode>
    <ShellRouter />
  </StrictMode>,
  document.getElementById("root")
);
