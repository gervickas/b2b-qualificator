import { Suspense, lazy } from 'react'
import { BrowserRouter, Route, Switch } from "react-router-dom";

const HistoryList = lazy(() => import('historyDomain/AppRouter'))

const ShellRouter = () => (
  <BrowserRouter>
    <Switch>
      <Suspense fallback={<div>Loading routers</div>}>
        <Route path="/history" component={HistoryList} exact />
      </Suspense>
    </Switch>
  </BrowserRouter>
)
export default ShellRouter;