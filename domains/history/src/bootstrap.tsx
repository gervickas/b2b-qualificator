import { StrictMode } from "react";
import { render }from "react-dom";
import AppRouter from "./components/Router";

render(
  <StrictMode>
    <AppRouter />
  </StrictMode>,
  document.getElementById("root")
);
