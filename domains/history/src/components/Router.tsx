import { Route, Switch, BrowserRouter as Router } from "react-router-dom";

import List from './List'

const AppRouter = () => (
    <Router>
        <Switch>
            <Route path="/history" component={List} exact />
        </Switch>
    </Router>
)

export default AppRouter